package edu.towson.cosc431.Magarin.todos

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.task_view.view.*
import java.util.*


class TodoAdapter(val todoList:List<Model>, val controller:IController) :RecyclerView.Adapter<TodoViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        //1. Inflate a view
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task_view, parent, false)


        //2. Create and return the ViewHolder
        val viewHolder = TodoViewHolder(view)

        //Handle the events
        viewHolder.itemView.isCompletecb.setOnClickListener {

            val position = viewHolder.adapterPosition

            controller.toggleComplete(todoList[position])
            notifyItemChanged(position)
        }


        viewHolder.itemView?.setOnLongClickListener {
            val position = viewHolder.adapterPosition
            controller.removePerson(todoList[position])
            notifyItemRemoved(position)

            notifyDataSetChanged()
            true

        }




            return viewHolder
    }

    override fun getItemCount(): Int {
        return todoList.size


}
    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {


        //1. get the song at the position
        val todo= todoList.get(position)



        //2. set the view properties

        holder.itemView.title_pass.text=todo.title

        holder.itemView.content_pass.text=todo.Contents

        holder.itemView.date_pass.text= Date().toString()

        holder.itemView.isCompletecb.isChecked = todo.IsCompleted


    }


}
class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view){

}