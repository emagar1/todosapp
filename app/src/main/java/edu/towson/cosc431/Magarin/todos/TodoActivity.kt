package edu.towson.cosc431.Magarin.todos

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_todo_activity.*
import kotlinx.android.synthetic.main.task_view.*
import java.util.*

class TodoActivity : AppCompatActivity() {






override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todo_activity)
        save_but.setOnClickListener{returnInfo()}

    }


    private fun returnInfo() {

        UUID.randomUUID()
        val info = Intent()
        val title = Title_in.editableText.toString()
        val content = contents.editableText.toString()
        val checkbox = checkBox.isChecked
        val dateCreated = Date().toString()


        val song= Model(UUID.randomUUID(),title, content,checkbox, dateCreated)// Date().toString())
        val gson = Gson()
        val json = gson.toJson(song)
        info.putExtra(TODO,json)
        setResult(Activity.RESULT_OK,info)
        finish()


    }
companion object {
    val TODO ="TODO"
}

}




