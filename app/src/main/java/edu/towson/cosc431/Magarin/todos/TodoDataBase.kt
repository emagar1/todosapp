package edu.towson.cosc431.Magarin.todos
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.*

// Database constants
object TodosContract { // the whole database
    object TodosEntry { // one table
        const val TABLE_NAME = "todos"
        const val COLUMN_NAME_ID = "todos_id"
        const val COLUMN_NAME_TITLE = "todos_title"
        const val COLUMN_NAME_CONTENT = "todos_content"
        const val COLUMN_NAME_DATECREATED = "date_created"
        const val COLUMN_NAME_COMPLETED = "todos_cmpleted"
        const val COLUMN_NAME_DELETED = "todos_deleted"
    }
}

// Interface for methods
interface IDatabase {
    fun addTodos(todos: Model)
    fun getTodos(): List<Model>
    fun deleteTodos(todos: Model)
    fun updateTodos(todos: Model)
    fun getCompleted(): List<Model>
    fun getActive(): List<Model>
    fun getTodo(id : String): Model
}

// SQLLite create entry
private const val SQL_CREATE_ENTRIES =
        "CREATE TABLE ${TodosContract.TodosEntry.TABLE_NAME} ( " +
                "${TodosContract.TodosEntry.COLUMN_NAME_ID} TEXT PRIMARY KEY, " +
                "${TodosContract.TodosEntry.COLUMN_NAME_TITLE} TEXT, " +
                "${TodosContract.TodosEntry.COLUMN_NAME_CONTENT} TEXT, " +
                "${TodosContract.TodosEntry.COLUMN_NAME_DATECREATED} TEXT, " +
                "${TodosContract.TodosEntry.COLUMN_NAME_DELETED} INTEGER DEFAULT 0, " +
                "${TodosContract.TodosEntry.COLUMN_NAME_COMPLETED} INTEGER DEFAULT 0)"

private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ${TodosContract.TodosEntry.TABLE_NAME}"

class TodosDatabase(ctx: Context) : IDatabase {

    override fun getActive(): List<Model> {
        // 1. select * from todos
        val cursor = db
                .rawQuery(
                        "SELECT * FROM ${TodosContract.TodosEntry.TABLE_NAME} "
                                // where not deleted
                                + "WHERE ${TodosContract.TodosEntry.COLUMN_NAME_COMPLETED}=0 AND ${TodosContract.TodosEntry.COLUMN_NAME_DELETED}=0"
                        , null
                )
        val result = mutableListOf<Model>()

        // 2. process the cursor
        while(cursor.moveToNext()) {
            val id: String = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_ID)
                    )

            val isCompletedAsInt = cursor
                    .getInt(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_COMPLETED)
                    )

            val title = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_TITLE)
                    )

            val content = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_CONTENT)
                    )

            val date = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_DATECREATED)
                    )

            val todos = Model(id = UUID.fromString(id)
                    , title = title
                    , Contents = content
                    , IsCompleted = isCompletedAsInt == 1
                    , date = date
            )
            result.add(todos)
        }

        // 2.5 close the cursor
        cursor.close()

        // 3. return the list
        return result

    }


    override fun getCompleted(): List<Model> {
        // 1. select * from todos
        val cursor = db
                .rawQuery(
                        "SELECT * FROM ${TodosContract.TodosEntry.TABLE_NAME} "
                                // where not deleted
                                + "WHERE ${TodosContract.TodosEntry.COLUMN_NAME_COMPLETED}=1 AND ${TodosContract.TodosEntry.COLUMN_NAME_DELETED}=0"
                        , null
                )
        val result = mutableListOf<Model>()

        // 2. process the cursor
        while(cursor.moveToNext()) {
            val id: String = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_ID)
                    )

            val isCompletedAsInt = cursor
                    .getInt(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_COMPLETED)
                    )

            val title = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_TITLE)
                    )

            val content = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_CONTENT)
                    )

            val date = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_DATECREATED)
                    )

            val todos = Model(id = UUID.fromString(id)
                    , title = title
                    , Contents = content
                    , IsCompleted = isCompletedAsInt == 1
                    , date = date
            )
            result.add(todos)
        }

        // 2.5 close the cursor
        cursor.close()

        // 3. return the list
        return result

    }

    override fun addTodos(todos: Model) {
        //1. Convert the Todos to ContentValues
        val contentValues = toContentValues(todos)

        //2. insert into the db
        db.insert(
                TodosContract.TodosEntry.TABLE_NAME, // table name
                null, // nullColumnHack -- look it up!
                contentValues
        )

    }

    override fun getTodo(id : String): Model {
        // 1. select * from todos
        val cursor = db
                .rawQuery(
                        "SELECT * FROM ${TodosContract.TodosEntry.TABLE_NAME} "
                                // where not deleted
                                + "WHERE ${TodosContract.TodosEntry.COLUMN_NAME_DELETED}=0 AND ${TodosContract.TodosEntry.COLUMN_NAME_ID}= ${id} "
                        , null
                )

        // 2. process the cursor
        lateinit var result: Model

        while(cursor.moveToNext()) {
            val id: String = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_ID)
                    )

            val isCompletedAsInt = cursor
                    .getInt(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_COMPLETED)
                    )

            val title = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_TITLE)
                    )

            val content = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_CONTENT)
                    )

            val date = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_DATECREATED)
                    )

            var todos = Model(id = UUID.fromString(id)
                    , title = title
                    , Contents = content
                    , IsCompleted = isCompletedAsInt == 1
                    , date = date
            )
            result = todos
        }

        // 2.5 close the cursor
        cursor.close()

        // 3. return the list
        return result

    }

    override fun getTodos(): List<Model> {
        // 1. select * from todos
        val cursor = db
                .rawQuery(
                        "SELECT * FROM ${TodosContract.TodosEntry.TABLE_NAME} "
                                // where not deleted
                                + "WHERE ${TodosContract.TodosEntry.COLUMN_NAME_DELETED}=0"
                        , null
                )
        val result = mutableListOf<Model>()

        // 2. process the cursor
        while(cursor.moveToNext()) {
            val id: String = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_ID)
                    )

            val isCompletedAsInt = cursor
                    .getInt(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_COMPLETED)
                    )

            val title = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_TITLE)
                    )

            val content = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_CONTENT)
                    )

            val date = cursor
                    .getString(
                            cursor.getColumnIndex(TodosContract.TodosEntry.COLUMN_NAME_DATECREATED)
                    )

            var todos = Model(id = UUID.fromString(id)
                    , title = title
                    , Contents = content
                    , IsCompleted = isCompletedAsInt == 1
                    , date = date
            )
            result.add(todos)
        }

        // 2.5 close the cursor
        cursor.close()

        // 3. return the list
        return result

    }

    override fun deleteTodos(todos: Model) {
        // 1. Convert to contentValues
        val deletedTodos = todos.copy(isDeleted = true)
        val contentValues = toContentValues(deletedTodos)

        // 2. Update db
        db.update(
                TodosContract.TodosEntry.TABLE_NAME,
                contentValues,
                "${TodosContract.TodosEntry.COLUMN_NAME_ID} = ?",
                arrayOf(deletedTodos.id.toString())
        )

    }

    override fun updateTodos(todos: Model) {
        // 1. Convert to contentValues
        val contentValues = toContentValues(todos)

        // 2. save to the db
        db.update(
                TodosContract.TodosEntry.TABLE_NAME,
                contentValues,
                "${TodosContract.TodosEntry.COLUMN_NAME_ID} = ?",
                arrayOf(todos.id.toString())
        )
    }

    private fun toContentValues(todos: Model): ContentValues {
        val cv = ContentValues()

        cv.put(TodosContract.TodosEntry.COLUMN_NAME_ID, todos.id.toString())
        cv.put(TodosContract.TodosEntry.COLUMN_NAME_TITLE, todos.title)
        cv.put(TodosContract.TodosEntry.COLUMN_NAME_CONTENT, todos.Contents)
        cv.put(TodosContract.TodosEntry.COLUMN_NAME_DATECREATED, todos.date)
        val isCompletedAsInt = when(todos.IsCompleted) {
            true -> 1
            false -> 0
        }
        cv.put(TodosContract.TodosEntry.COLUMN_NAME_COMPLETED, isCompletedAsInt)
        val isDeletedAsInt = when(todos.isDeleted) {
            true -> 1
            false -> 0
        }
        cv.put(TodosContract.TodosEntry.COLUMN_NAME_DELETED, isDeletedAsInt)

        return cv
    }

    private val db: SQLiteDatabase

    init {
        db = TodosDbHelper(ctx).writableDatabase
    }

    class TodosDbHelper(ctx: Context) : SQLiteOpenHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION)
    {
        override fun onCreate(db: SQLiteDatabase?) {
            db?.execSQL(SQL_CREATE_ENTRIES)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            db?.execSQL(SQL_DELETE_ENTRIES)
            onCreate(db)
        }

        companion object {
            val DATABASE_NAME = "todos.db"
            val DATABASE_VERSION = 10
        }
    }


}