package edu.towson.cosc431.Magarin.todos

import com.google.gson.Gson
import java.util.*

data class Model (     val id: UUID,
                    val title: String,
                  val Contents:String,
                  val IsCompleted:Boolean,

                  val date: String,
                       val isDeleted: Boolean = false){
    /**
     * Uses Gson to serialize the song into json
     */
    fun toJson(): String {
        return Gson().toJson(this)
    }


    fun isValid(): Boolean {
        return title.isNotEmpty() && Contents.isNotEmpty()
    }


    fun getError(): String {
        if(title.isEmpty()) {
            return "Song name is empty"
        }
        if(Contents.isEmpty()) {
            return "Song artist is empty"
        }

        throw Exception("Valid")
    }



}
data class ApiResult(val page: Int, val per_page: Int, val total: Int, val total_pages: Int,
                     val data: List<Model>)