package edu.towson.cosc431.Magarin.todos

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.task_view.*
import java.io.*
import java.net.URL
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import javax.net.ssl.HttpsURLConnection

class MainActivity : AppCompatActivity(),IController {


    override fun removePerson(todo: Model) {
        db.deleteTodos(todo)
        todoList.clear()
        todoList.addAll(db.getTodos())
        recyclerView.adapter?.notifyDataSetChanged()

    }


    override fun addtodo(todo: Model) {
        db.addTodos(todo)
        todoList.clear()
        todoList.addAll(db.getTodos())
        recyclerView.adapter?.notifyDataSetChanged()

    }

    lateinit var db: IDatabase


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        db = TodosDatabase(this)


        recyclerView.layoutManager = LinearLayoutManager(this)


        startbtn.setOnClickListener { activity() }

        populatetodoList()

        todoList.addAll(db.getTodos())
        val todosAdapter = TodoAdapter(todoList, this)
        recyclerView.adapter = todosAdapter

        (recyclerView.adapter as TodoAdapter).notifyDataSetChanged()
    }

    private fun activity() {

        val intent = Intent()
        intent.component = ComponentName(this, TodoActivity::class.java)

        startActivityForResult(intent, LIST)


    }


    override fun toggleComplete(todo: Model) {

        val newSong = todo.copy(IsCompleted = !todo.IsCompleted)
        db.updateTodos(newSong)
        todoList.clear()
        todoList.addAll(db.getTodos())
        recyclerView.adapter?.notifyDataSetChanged()


    }

    var todoList: MutableList<Model> = mutableListOf()


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            LIST -> {
                if (resultCode == Activity.RESULT_OK) {
                    val json = data?.getStringExtra(TodoActivity.TODO) as String?
                    Log.i("Main Activity", json)
                    if (json != null) {
                        val todo = Gson().fromJson(json, Model::class.java)
                     //   todoList.add(song)
                        addtodo(todo)
                        Log.i("Info", json)

                        recyclerView.adapter?.notifyDataSetChanged()
                    }
                } else {
                    Log.i("Main Activity", "Wrong input")
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if(executor.isShutdown) {
            executor = Executors.newFixedThreadPool(4) as ThreadPoolExecutor
        }
    }

    override fun onPause() {
        super.onPause()
        // cancel any threads on pause
        executor.shutdownNow()
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // cache the user list
        outState?.putString(USERS_KEY, Gson().toJson(userList))
    }

    private fun fetchUsers() {
        // this must be done on a background thread
        executor.execute {
            val result = fetch(API_URL)
            val apiResult = Gson().fromJson<ApiResult>(result, ApiResult::class.java)
            userList.addAll(apiResult.data)
            // update the view on the UI thread
            handler.post {
                recyclerView.adapter.notifyDataSetChanged()
            }
        }
    }

    override fun fetchAvatar(url: String, callback: (Bitmap) -> Unit) {
        executor.execute {
            var bitmap: Bitmap? = null
            bitmap = checkCache(url)
            if(bitmap == null) {
                val url_url = URL("${url}?delay=3")
                val connection = url_url.openConnection() as HttpsURLConnection?
                if (connection == null) {
                    //callback(null)
                } else {
                    connection.doInput = true
                    connection.requestMethod = "GET"
                    connection.connect()
                    bitmap = BitmapFactory.decodeStream(connection.inputStream)
                    cacheFile(url, bitmap)
                    // make sure the callback runs on the UI thread
                    handler.post {
                        callback(bitmap)
                    }
                }
            } else {
                handler.post {
                    callback(bitmap)
                }
            }
        }
    }

    private fun cacheFile(url: String, bitmap: Bitmap?) {
        if(bitmap != null) {
            getLast2PathSegmentsAsString(url)?.let { filename ->
                File.createTempFile(filename, null, cacheDir)
                val cacheFile = File(cacheDir, filename)
                val fos = FileOutputStream(cacheFile)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
                fos.close()
            }
        }
    }

    private fun getLast2PathSegmentsAsString(url: String) : String? {
        val segments = Uri.parse(url)?.pathSegments
        val last2 = segments?.subList(segments.size - 2, segments.size)
        return last2?.reduce { acc, s -> acc + s }
    }

    private fun checkCache(url: String): Bitmap? {
        getLast2PathSegmentsAsString(url)?.let { filename ->
            val cacheFile = File(cacheDir, filename)
            if(cacheFile.exists()) {
                return BitmapFactory.decodeStream(cacheFile.inputStream())
            } else {
                return null
            }
        }
        return null
    }

    companion object {
        val USERS_KEY = "users"
        val API_URL = "https://reqres.in/api/users"
    }

    private fun fetch(url: String): String? {
        val url = URL("${url}?per_page=12&page=1&delay=3")
        val connection = url.openConnection() as HttpsURLConnection?
        if(connection == null) return null
        connection.doInput = true
        connection.requestMethod = "GET"
        connection.connect()
        return readStream(connection.inputStream, 5000)
    }

    /**
     * Converts the contents of an InputStream to a String.
     */
    @Throws(IOException::class, UnsupportedEncodingException::class)
    fun readStream(stream: InputStream, maxReadSize: Int): String? {
        val reader: Reader? = InputStreamReader(stream, "UTF-8")
        val rawBuffer = CharArray(maxReadSize)
        val buffer = StringBuffer()
        var readSize: Int = reader?.read(rawBuffer) ?: -1
        var maxReadBytes = maxReadSize
        while (readSize != -1 && maxReadBytes > 0) {
            if (readSize > maxReadBytes) {
                readSize = maxReadBytes
            }
            buffer.append(rawBuffer, 0, readSize)
            maxReadBytes -= readSize
            readSize = reader?.read(rawBuffer) ?: -1
        }
        stream.close()
        return buffer.toString()
    }
}

interface IController {
    val userList: List<Model>
    fun fetchAvatar(url: String, callback: (Bitmap)->Unit)
}

    companion object {
        val LIST = 100
    }


    private fun populatetodoList() {
        val todos = db.getTodos()
        if (todos.isEmpty()) {
            (1..10).forEach {
                db.addTodos(Model(UUID.randomUUID(), "Android", "Recycler View", it % 3 == 0, date = String()))


                /*     todoList.add(Model(UUID.randomUUID(),"Title"+it, "Contents"+it,  it%3==0,date = Date()))
                 todoList.add(Model("Noooo", "Yesss",  it%3==0,date = Date())) todoList.add(Model("Go to work", "Sleep",  it%3==0,date = Date()))
                  todoList.add(Model("Go to School", "Awake",  it%3==0,date = Date()))
                  todoList.add(Model("Practice Guitar", "Scales",  it%3==0,date = Date()))
                  todoList.add(Model("Math Homework", "Problem3",  it%3==0,date = Date()))
                  todoList.add(Model("COSC Hmwk", "Todo",  it%3==0,date = Date()))
                  todoList.add(Model("English HMWK", "Short Story",  it%3==0,date = Date()))
                  todoList.add(Model("Android", "Recycler View",  it%3==0,date = Date()))
                  */
            }
        }


    }


}